import sqlite3


conn = sqlite3.connect('temperatures.db')

conn.execute('''CREATE TABLE Temperatures(
   ID     INT              NOT NULL,
   TEMP   INT              NOT NULL,
   DDATE  DATE             NOT NULL,  
   PRIMARY KEY (ID));''')

conn.close()

print('Database created successfully');

