# coding: utf-8

# TODO:
# musisz tutaj dopisas/dokleic swój kawałek kodu
# odpowiedzialny za pobieranie temperatury
# ten skrypt będzie odpalany co 24h i będzie aktualizował wpis w bazie danych
# dokładnie będziemy co 24h zapisywać temperature na dany dzień
# baza posiada jedną tabele o nazwie temperatures
# tabela składa się z pól
# ID - automatycznie generowane
# TEMP - temperature w liczbach całkowitych np. 1, 2, -3, -5
# DDATE - data w formacie YYYY-DD-MM

import schedule
import time
import datetime

from utils import sqlite_insert, sqlite_select_max_id


def job():
    conn = sqlite3.connect('temperatures.db')

    now = datetime.datetime.now()
    maxid = sqlite_select_max_id(conn, 'temperatures') + 1

    sqlite_insert(conn, 'temperatures', {
        'id': maxid,
        'temp': 0,  # TODO: tutaj należy podać temperature z czujnika
        'ddate': '{0}-{1}-{2}'.format(now.year, now.month, now.day)})

    conn.close()

schedule.every().day.at('15:00').do(job, 'it is 15:00')


while True:
    schedule.run_pending()
    time.sleep(60)

