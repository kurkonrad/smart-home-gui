import sqlite3

from PyQt4 import QtCore, QtGui, uic

from utils import sqlite_select
from dialog2 import Dialog2


DIALOG_UI = uic.loadUiType('templates/dialog1.ui')[0]


class Dialog1(QtGui.QDialog, DIALOG_UI):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi(self)

        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL('clicked()'), self.dialog_chart)

        table = self.listView_2

        conn = sqlite3.connect('temperatures.db')
        rows = sqlite_select(conn, 'temperatures')
        conn.close()

        table.setColumnCount(2)
        table.setHorizontalHeaderLabels(['Temperatura', 'Data'])
        table.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.Stretch)

        table.setRowCount(len(rows))

        index = 0
        for row in rows:
            table.setItem(index, 0, QtGui.QTableWidgetItem('{0}'.format(row[1])))
            table.setItem(index, 1, QtGui.QTableWidgetItem('{0}'.format(row[2])))

            index += 1

        table.show()

    def dialog_chart(self):
        dialog = Dialog2()

