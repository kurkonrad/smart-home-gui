import sqlite3


def sqlite_insert(conn, table, row):
    cols = ', '.join('"{}"'.format(col) for col in row.keys())
    vals = ', '.join(':{}'.format(col) for col in row.keys())

    sql = 'INSERT INTO "{0}" ({1}) VALUES ({2})'.format(table, cols, vals)

    conn.cursor().execute(sql, row)
    conn.commit()


def sqlite_select(conn, table):
    cur = conn.cursor()
    cur.execute('SELECT * FROM {table}'.format(table=table))
 
    rows = cur.fetchall()
 
    return rows


def sqlite_select_max_id(conn, table):
    cur = conn.cursor()
    cur = sqlite3.execute('SELECT max(ID) FROM {table}'.format(table=table))

    maxid = cursor.fetchone()[0]

    return maxid


STYLESHEET = {
    'buttons': {
        'default': 'color: #292b2c; background-color: #fff; border-color: #ccc;',
        'danger': 'color: #fff; background-color: #d9534f; border-color: #d9534f;',
        'success': 'color: #fff; background-color: #449d44; border-color: 1px solid #d9534f;'
    }
}

