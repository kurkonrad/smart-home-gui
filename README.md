## instalacja
#### tworzymy bazę danych
```sh
$ python fixtures/fixtures.py
```
#### odpalamy program do odczytu temp. ( co 24h, o 15:00 )
```sh
$ python management.py
```
#### odpalamy program
```sh
$ python main.py
```

## wymagania
* Python 3
* Qt4 >= 4.8
