import sqlite3
import matplotlib.pyplot as plt
import numpy as np

from PyQt4 import QtCore, QtGui, uic

from utils import sqlite_select

plt.rcdefaults()


class Dialog2():
    def __init__(self):
        conn = sqlite3.connect('temperatures.db')
        rows = sqlite_select(conn, 'temperatures')
        conn.close()

        labels = []
        temperatures = []
        for row in rows:
            labels.append(row[2])
            temperatures.append(row[1])

        labels = tuple(labels)
        y_pos = np.arange(len(labels))
	 
	plt.bar(y_pos, temperatures, align='center', alpha=0.5)
	plt.xticks(y_pos, labels)
	plt.ylabel('Temperatura')
	plt.title('Wykres temperatury')
	 
	plt.show()

