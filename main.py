# coding: utf-8

import sys
import datetime
import sqlite3
from random import randint

from PyQt4 import QtCore, QtGui, uic

# import RPi.GPIO as GPIO

from utils import STYLESHEET
from dialog1 import Dialog1

WINDOW_UI = uic.loadUiType('templates/mainwindow.ui')[0]


class MyWindow(QtGui.QMainWindow, WINDOW_UI):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.setupUi(self)

        # GPIO.setmode(GPIO.BCM)
        # GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
        # GPIO.setup(20, GPIO.OUT)
        # GPIO.setup(25, GPIO.OUT)

        QtCore.QObject.connect(self.btn_kitchen_on, QtCore.SIGNAL('clicked()'), self.kitchen_on)
        QtCore.QObject.connect(self.btn_kitchen_off, QtCore.SIGNAL('clicked()'), self.kitchen_off)
        self.btn_kitchen_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_kitchen_off.setStyleSheet(STYLESHEET['buttons']['default'])

        QtCore.QObject.connect(self.btn_alarm_on, QtCore.SIGNAL('clicked()'), self.alarm_on)
        QtCore.QObject.connect(self.btn_alarm_off, QtCore.SIGNAL('clicked()'), self.alarm_off)
        self.btn_alarm_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_alarm_off.setStyleSheet(STYLESHEET['buttons']['default'])

        QtCore.QObject.connect(self.btn_blinds_on, QtCore.SIGNAL('clicked()'), self.blinds_on)
        QtCore.QObject.connect(self.btn_blinds_off, QtCore.SIGNAL('clicked()'), self.blinds_off)
        self.btn_blinds_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_blinds_off.setStyleSheet(STYLESHEET['buttons']['default'])

        QtCore.QObject.connect(self.action_quit, QtCore.SIGNAL('triggered()'), self.close_window)
        QtCore.QObject.connect(self.action_history, QtCore.SIGNAL('triggered()'), self.dialog_history)

        QtCore.QObject.connect(self.btn_ok, QtCore.SIGNAL('clicked()'), self.set_temperature)
        self.btn_ok.setStyleSheet(STYLESHEET['buttons']['default'])

        now = datetime.datetime.now()
        self.statusBar().showMessage('{0}-{1}-{2}'.format(now.year, now.month, now.day))

        self.update_temperature()
        self.show()

    def set_temperature(self):
        # TODO:
        # wartość z inputa możemy pobrać: self.input_temperature.text()
        # tutaj musisz dopisać sterowanie piecem
        print(self.input_temperature.text())

    def close_window(self):
        self.close()
    
    def dialog_history(self):
        dialog = Dialog1()
        dialog.exec_()

    def kitchen_on(self):
        # GPIO.output(20, GPIO.HIGH)

        self.btn_kitchen_on.setStyleSheet(STYLESHEET['buttons']['success'])
        self.btn_kitchen_off.setStyleSheet(STYLESHEET['buttons']['default'])
        self.update_temperature()

    def kitchen_off(self):
        # GPIO.output(20,GPIO.LOW)

        self.btn_kitchen_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_kitchen_off.setStyleSheet(STYLESHEET['buttons']['danger'])
        self.update_temperature()

    def alarm_on(self):
        self.btn_alarm_on.setStyleSheet(STYLESHEET['buttons']['success'])
        self.btn_alarm_off.setStyleSheet(STYLESHEET['buttons']['default'])
        self.update_temperature()

        # if GPIO.input(12)== GPIO.LOW:
        #     GPIO.output(25, GPIO.HIGH)
        # else:
        #     GPIO.output(25,GPIO.LOW)


    def alarm_off(self):
        self.btn_alarm_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_alarm_off.setStyleSheet(STYLESHEET['buttons']['danger'])
        self.update_temperature()

        # GPIO.output(25, GPIO.LOW)

    def blinds_on(self):
        self.btn_blinds_on.setStyleSheet(STYLESHEET['buttons']['success'])
        self.btn_blinds_off.setStyleSheet(STYLESHEET['buttons']['default'])

    def blinds_off(self):
        self.btn_blinds_on.setStyleSheet(STYLESHEET['buttons']['default'])
        self.btn_blinds_off.setStyleSheet(STYLESHEET['buttons']['danger'])
        self.update_temperature()

    def update_temperature(self):
        # TODO:
        # tutaj będziemy pobierać temp. dla danego dnia
        # ale co w sytuacji gdy temp. dla danego dnia jeszcze nie będzie?
        self.lcd_temperature.display('{0}'.format(randint(-15, 34)))


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = MyWindow()

    # GPIO.cleanup()

    sys.exit(app.exec_())

